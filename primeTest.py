def primeTest(x):
	for divisor in range(2,x):
		if x%divisor == 0:
			# X is non-prime
			return False
			
	return True
	
print(primeTest(7))
print(primeTest(10))