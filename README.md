# About this Code

The code in this repository is part of a tutorial by **0612 TV** entitled **Prime Factorization Algorithm!**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/kM_y15bYB8s/0.jpg)](https://www.youtube.com/watch?v=kM_y15bYB8s "Click to Watch")


# Thanks To

* YouTube user **@pigizoid9924** for discovering a bug that produced wrong answers in primefactors_extended.py
* YouTube user **Dat_Kowalski** for revealing an inefficiency with the `isPrime()` function in factorizer_extended.py
* YouTube user **denifednu** for discussing the slowness of the print statement in primefactors_extended.py


# License

This project is licensed under the **Apache License 2.0**. For more details, please refer to LICENSE.txt within this repository.